﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Bubbly.Utility.Pooling
{
    public class PoolingService
    {
        Dictionary<Type, object> availablePools;

        public PoolingService()
        {
            availablePools = new Dictionary<Type, object>();
        }

        public ObjectPool<T> GetPool<T>() where T : IPoolable<T>
        {
            if (!availablePools.ContainsKey(typeof(T)))
            {
                throw new KeyNotFoundException("Object pool for the given type not found. Make sure you have registered one");
            }
            return availablePools[typeof(T)] as ObjectPool<T>;
        }

        public void RegisterPool<T>(Func<T> creationLogic) where T : IPoolable<T>
        {
            availablePools.Add(typeof(T), new ObjectPool<T>(creationLogic));
        }

        public void RegisterPool<T>(T prefab) where T : MonoBehaviour, IPoolable<T>
        {
            availablePools.Add(typeof(T), new ObjectPool<T>(delegate { return GameObject.Instantiate(prefab); }));
        }
    }
}
