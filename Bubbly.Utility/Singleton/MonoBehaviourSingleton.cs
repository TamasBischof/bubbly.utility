﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Bubbly.Utility.Singleton
{
    /// <summary>
    /// A singleton deriving from MonoBehaviour. If instance is null, a new instance is instantiated on an empty gameobject on the scene root.
    /// Does not prevent manual instantiation.
    /// </summary>
    /// <typeparam name="T">Type deriving from MonoBehaviour.</typeparam>
    public class MonoBehaviourSingleton<T> : MonoBehaviour where T : MonoBehaviourSingleton<T>
    {
        static T instance;

        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = GameObject.FindObjectOfType<T>();
                    if (instance == null)
                    {
                        var obj = new GameObject();
                        instance = obj.AddComponent<T>();
                    }
                }

                return instance;
            }
        }
    }
}
