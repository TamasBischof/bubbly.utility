﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bubbly.Utility.Singleton
{
    /// <summary>
    /// Simple singleton implementation. Derive from this for singleton behavior.
    /// MonoBehaviours should be derived from MonoBehaviourSingleton or PersistentSingleton instead.
    /// Not thread-safe. Does not prevent manual instantiation.
    /// </summary>
    /// <typeparam name="T">A type that has a parameterless constructor.</typeparam>
    public class Singleton<T> where T : Singleton<T>, new()
    {
        static T instance;

        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new T();
                }

                return instance;
            }
        }
    }
}
