﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Bubbly.Utility.Singleton
{
    /// <summary>
    /// A singleton that doesn't get destroyed between scene transitions. Does not prevent manual instantiation.
    /// </summary>
    /// <typeparam name="T">A type deriving from MonoBehaviour.</typeparam>
    public class PersistentSingleton<T> : MonoBehaviour where T : PersistentSingleton<T>
    {
        static T instance;

        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = GameObject.FindObjectOfType<T>();
                    if (instance == null)
                    {
                        var obj = new GameObject() { name = typeof(T).Name };
                        instance = obj.AddComponent<T>();
                    }
                    GameObject.DontDestroyOnLoad(instance);
                }

                return instance;
            }
        }
    }
}
