﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bubbly.Utility
{
    /// <summary>
    /// Extensions for selecting elements at random, shuffling, and list manipulation.
    /// </summary>
    public static class RandomizeExtensions
    {
        static System.Random rand = new System.Random();

        /// <summary>
        /// Extension method to rearrange a list randomly.
        /// </summary>
        /// <typeparam name="T">Generic type parameter of the collection.</typeparam>
        /// <param name="collection">Target IList.</param>
        public static void Shuffle<T>(this IList<T> collection)
        {
            int n = collection.Count;
            while (n > 1)
            {
                n--;
                int k = rand.Next(n + 1);
                T value = collection[k];
                collection[k] = collection[n];
                collection[n] = value;
            }
        }

        /// <summary>
        /// Extension method to remove elements at random from a collection that implements IList.
        /// </summary>
        /// <typeparam name="T">Generic type parameter of the collection.</typeparam>
        /// <param name="collection">Target IList.</param>
        /// <param name="toRemove">Number of elements to remove.</param>
        public static void RemoveRandom<T>(this IList<T> collection, int toRemove)
        {
            for (int i = 0; i < toRemove; i++)
            {
                collection.RemoveAt(rand.Next(collection.Count));
            }
        }

        /// <summary>
        /// Returns a random element of an IList.
        /// </summary>
        /// <typeparam name="T">Generic type parameter of the collection.</typeparam>
        /// <param name="collection">Target collection.</param>
        /// <returns>A randomly chosen element of the collection.</returns>
        public static T GetRandom<T>(this IList<T> collection)
        {
            return collection[rand.Next(collection.Count())];
        }

        /// <summary>
        /// Picks a random character from a string.
        /// </summary>
        /// <param name="str">Target string.</param>
        /// <returns>A randomly chosen character from the supplied string.</returns>
        public static char GetRandom(this string str)
        {
            return str[rand.Next(str.Length)];
        }
    }
}
