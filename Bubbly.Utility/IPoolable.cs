﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bubbly.Utility.Pooling
{
    public interface IPoolable<T>
    {
        void Reset();
        /// <summary>
        /// Implementing classes should perform cleanup code, then raise this event. Pools using objects of this interface should handle the event accordingly.
        /// </summary>
        event Action<T> Release;
    }
}
