﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Bubbly.Utility
{
    /// <summary>
    /// Contains extensions methods that extend Transform funcionality.
    /// </summary>
    public static class TransformExtensions
    {
        /// <summary>
        /// Extension method to destroy all children of a Transform.
        /// </summary>
        /// <param name="transform">The target Transform</param>
        /// <remarks>Pretty dark name :/</remarks>
        public static void DestroyChildren(this Transform transform)
        {
            foreach (Transform child in transform)
            {
                GameObject.Destroy(child.gameObject);
            }
            transform.DetachChildren(); //detach children from the parent so that parent.childcount gets updated (Unity only destroys the child transforms at the end of the frame)
        }

        /// <summary>
        /// Updates the position of a Transform in world space without changing its Z value. Useful for 2D games.
        /// </summary>
        /// <param name="position">New position for the transform.</param>
        public static void Move2D(this Transform transform, Vector2 position)
        {
            transform.position = new Vector3(position.x, position.y, transform.position.z);
        }

        /// <summary>
        /// Updates the position of a Transform in local space without changing its Z value. Useful for 2D games.
        /// </summary>
        /// <param name="position">New position for the transform.</param>
        public static void MoveLocal2D(this Transform transform, Vector2 position)
        {
            transform.localPosition = new Vector3(position.x, position.y, transform.position.z);
        }
        
    }
}
