﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bubbly.Utility
{
    public static class CollectionExtensions
    {
        /// <summary>
        /// Iterator that supplies items from an IList collection. Wraps around at the end.
        /// </summary>
        /// <param name="collection">Collection to iterate over.</param>
        public static IEnumerator<T> CyclingIterator<T>(this IList<T> collection)
        {
            for (int i = 0; i <= collection.Count(); i++)
            {
                if (i == collection.Count())
                {
                    i = 0;
                }
                yield return collection[i];
            }
        }
    }
}
