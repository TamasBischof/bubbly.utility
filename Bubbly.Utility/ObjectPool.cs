﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Bubbly.Utility.Pooling
{
    /// <summary>
    /// Represents a generic object pool.
    /// </summary>
    /// <typeparam name="T">Type of the pooled objects.</typeparam>
    public class ObjectPool<T> where T : IPoolable<T>
    {
        private Stack<T> pool;
        private Func<T> creationLogic;

        public ObjectPool(Func<T> creationLogic)
        {
            pool = new Stack<T>();
            this.creationLogic = creationLogic;
        }

        public T Get()
        {
            if (pool.Count == 0)
            {
                var newItem = creationLogic();
                newItem.Release += Reclaim;
                pool.Push(newItem);
            }

            var result = pool.Pop();
            return result;
        }

        private void Reclaim(T obj)
        {
            obj.Reset();
            pool.Push(obj);
        }
    }
}
